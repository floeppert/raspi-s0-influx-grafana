S0 Counter for Raspberry Pi with InfluxDB and Grafana
=====================================================

This is a little Project to Visualize my Energy Consumption

I have a Energy Counter with S0 output. On this Energy Meter is a 1-Wire S0 Counter.

This 1-Wire counter is connected with Raspberry Pi GPIO and on the Raspio, 1Wireis enabled.


Deploy Influxdb and Grafana with docker-compose
-----------------------------------------------

Use the docker-compose.yaml file

Python Scripts
--------------

These are for writing the Data in the Influxdb.
You must create the Database manually.

The Scirpts are executed throught Cron-Jobs

Grafana
-------

grafana_dashboard is a example Dashboard
