import os
import re
import time
import datetime
from influxdb import InfluxDBClient

host = "localhost"
port = 8086
dbname = 'S0'


# Read Lines in File
f = open('/sys/bus/w1/devices/1d-0000000fd6db/w1_slave', 'r')
lines = f.readlines()
f.close()

#Choose corect line
for line in lines:
    if line.startswith('35 '):
        rawvalue = line

# extract value from line
value = re.match('.*c=(\d*).*',rawvalue)
value = float(value.group(1))

client = InfluxDBClient(host=host, port=port, database=dbname)
timestamp=datetime.datetime.utcnow().isoformat()
datapoints = [
        {
            "measurement": "S0_5m",
            "time": timestamp,
            "fields": {
                "count": value
            }
        }
        ]

bResult=client.write_points(datapoints)

